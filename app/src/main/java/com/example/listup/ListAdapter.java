package com.example.listup;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder> {
    private Context mContext;
    private SQLiteDatabase mDatabase;
    private Cursor mCursor;
    private Intent newIntent;

    public ListAdapter(Context context, Cursor cursor) {
        mContext = context;
        mCursor = cursor;
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        public TextView textName;
        public CheckBox checkBox;

        public ListViewHolder(@NonNull View listView) {
            super(listView);

            textName = listView.findViewById(R.id.textView_name_list);
            checkBox = listView.findViewById(R.id.checkList);
        }
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.list_info, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
        if (!mCursor.moveToPosition(position)) {
            return;
        }

        String name = mCursor.getString(mCursor.getColumnIndex(DatabaseContract.ListEntry.COLUMN_NAME));
        final long id = mCursor.getLong(mCursor.getColumnIndex(DatabaseContract.ListEntry._ID));
        final long userId = mCursor.getLong(mCursor.getColumnIndex(DatabaseContract.ListEntry.COLUMN_USER_ID));
        final boolean isCompleted = mCursor.getInt(mCursor.getColumnIndex(DatabaseContract.ItemEntry.COLUMN_IS_COMPLETED)) > 0;

        holder.textName.setText(name);
        holder.itemView.setTag(id);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newIntent = new Intent(mContext, ItemListActivity.class);

                Bundle b = new Bundle();
                b.putLong("listId", id);
                b.putLong("userId", userId);
                newIntent.putExtras(b);

                mContext.startActivity(newIntent);
            }
        });

        holder.checkBox.setOnCheckedChangeListener(null);

        //if true, your checkbox will be selected, else unselected
        holder.checkBox.setChecked(isCompleted);

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //set your object's last status
                DatabaseHelper databaseHelper = new DatabaseHelper(mContext);
                mDatabase = databaseHelper.getWritableDatabase();

                setIsCompletedForAllItems(id, isChecked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCursor.getCount();
    }

    public void swapCursor(Cursor newCursor) {
        if(mCursor != null) {
            mCursor.close();
        }

        mCursor = newCursor;

        if (newCursor != null) {
            notifyDataSetChanged();
        }
    }

    private void setIsCompletedForAllItems(long listId, boolean isCompleted){
        ContentValues cv = new ContentValues();
        cv.put(DatabaseContract.ItemEntry.COLUMN_IS_COMPLETED, isCompleted);

        mDatabase.update(DatabaseContract.ItemEntry.TABLE_NAME, cv, DatabaseContract.ItemEntry.COLUMN_LIST_ID + "=" + listId, null);
        mDatabase.update(DatabaseContract.ListEntry.TABLE_NAME, cv, DatabaseContract.ListEntry._ID + "=" + listId, null);
    }

}

