package com.example.listup;

import android.provider.BaseColumns;

class DatabaseContract {

    private DatabaseContract() {}

    static final class UserEntry implements BaseColumns {
        static final String TABLE_NAME = "user";
        static final String COLUMN_USERNAME = "username";
        static final String COLUMN_PASSWORD = "password";
    }

    static final class ListEntry implements BaseColumns {
        static final String TABLE_NAME = "list";
        static final String COLUMN_NAME = "name";
        static final String COLUMN_IS_COMPLETED = "is_completed";
        static final String COLUMN_TIMESTAMP = "timestamp";
        static final String COLUMN_USER_ID = "user_id";
    }

    static final class ItemEntry implements BaseColumns {
        static final String TABLE_NAME = "item_info";
        static final String COLUMN_NAME = "name";
        static final String COLUMN_AMOUNT = "amount";
        static final String COLUMN_IS_COMPLETED = "is_completed";
        static final String COLUMN_TIMESTAMP = "timestamp";
        static final String COLUMN_LIST_ID = "list_id";
    }
}
