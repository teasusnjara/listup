package com.example.listup;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.allyants.notifyme.NotifyMe;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ItemListActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    Calendar calendar = Calendar.getInstance();
    TimePickerDialog timePickerDialog;
    DatePickerDialog datePickerDialog;
    private SQLiteDatabase mDatabase;
    private ItemAdapter mAdapter;
    private EditText mEditTextName;
    private TextView mTextViewAmount;
    private int amount = 0;
    private long listId = 0;
    private long userId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_list);

        DatabaseHelper dbHelper = new DatabaseHelper(this);
        mDatabase = dbHelper.getWritableDatabase();

        Bundle b = getIntent().getExtras();
        if(b != null) {
            listId = b.getLong("listId");
            userId = b.getLong("userId");
        }

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new ItemAdapter(this, getAllItems());
        recyclerView.setAdapter(mAdapter);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT |
                ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                removeItem((long) viewHolder.itemView.getTag());
            }
        }).attachToRecyclerView(recyclerView);

        mEditTextName = findViewById(R.id.editText_name);
        mTextViewAmount = findViewById(R.id.textView_amount);

        Button buttonIncrease = findViewById(R.id.button_increase);
        Button buttonDecrease = findViewById(R.id.button_decrease);
        Button buttonAdd = findViewById(R.id.button_add);
        Button buttonBack = findViewById(R.id.back);
        Button buttonLogout = findViewById(R.id.logout);
        Button buttonNotification = findViewById(R.id.notification);

        buttonIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                increase();
            }
        });

        buttonDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decrease();
            }
        });

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addItem();
            }
        });

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent listIntent = new Intent(ItemListActivity.this, ListActivity.class);

                Bundle b = new Bundle();
                b.putLong("userId", userId);
                listIntent.putExtras(b);

                startActivity(listIntent);
            }
        });

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ItemListActivity.this);
                builder.setTitle("Confirmation PopUP!").setMessage("Are you sure you want to logout?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent loginIntent = new Intent(ItemListActivity.this, Login.class);

                        startActivity(loginIntent);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        buttonNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show(getFragmentManager(), "DatePickerDialog");
            }
        });

        datePickerDialog = DatePickerDialog.newInstance(
                ItemListActivity.this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );

        timePickerDialog = TimePickerDialog.newInstance(
                ItemListActivity.this,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                calendar.get(Calendar.SECOND),
                false
        );
    }

    private void increase() {
        amount++;
        mTextViewAmount.setText(String.valueOf(amount));
    }

    private void decrease() {
        if (amount > 0){
            amount--;
            mTextViewAmount.setText(String.valueOf(amount));
        }
    }

    private void addItem() {

        if (mEditTextName.getText().toString().trim().length() == 0 || amount == 0) {
            return;
        }

        String name = mEditTextName.getText().toString();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseContract.ItemEntry.COLUMN_NAME, name);
        contentValues.put(DatabaseContract.ItemEntry.COLUMN_AMOUNT, amount);
        contentValues.put(DatabaseContract.ItemEntry.COLUMN_LIST_ID, listId);
        contentValues.put(DatabaseContract.ItemEntry.COLUMN_IS_COMPLETED, 0);

        mDatabase.insert(DatabaseContract.ItemEntry.TABLE_NAME, null, contentValues);
        mAdapter.swapCursor(getAllItems());

        mEditTextName.getText().clear();
    }

    private void removeItem(long id) {
        mDatabase.delete(DatabaseContract.ItemEntry.TABLE_NAME,
                DatabaseContract.ItemEntry._ID + "=" + id, null);
        mAdapter.swapCursor(getAllItems());
    }

    private Cursor getAllItems() {
        return mDatabase.query(
                DatabaseContract.ItemEntry.TABLE_NAME,
                null,
                DatabaseContract.ItemEntry.COLUMN_LIST_ID + "=" + listId,
                null,
                null,
                null,
                DatabaseContract.ItemEntry.COLUMN_TIMESTAMP + " DESC"
        );
    }

    public void onDateSet (DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        timePickerDialog.show(getFragmentManager(), "TimePickerDialog");
    }

    public void onTimeSet (TimePickerDialog view, int hourOFDay, int minute, int second) {
        calendar.set(Calendar.HOUR_OF_DAY, hourOFDay);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);

        Cursor listCursor = mDatabase.rawQuery("SELECT * FROM list WHERE _id = ? LIMIT 1", new String[]{String.valueOf(listId)});
        String listName = "";
        if(listCursor.moveToFirst())
        {
            listName = listCursor.getString(listCursor.getColumnIndex(DatabaseContract.ListEntry.COLUMN_NAME));
        }

        Cursor itemList = mDatabase.rawQuery("SELECT * FROM item_info WHERE list_id = ? AND is_completed = ?", new String[]{String.valueOf(listId), "0"});
        List<String> itemNameList = new ArrayList<String>();

        while (itemList.moveToNext()) {
            itemNameList.add(itemList.getColumnIndex(DatabaseContract.ItemEntry.COLUMN_AMOUNT) + " x " + itemList.getString(itemList.getColumnIndex(DatabaseContract.ItemEntry.COLUMN_NAME)));
        }

        String content = String.join(", ", itemNameList);

        NotifyMe notifyMe = new NotifyMe.Builder(getApplicationContext())
                .title(listName)
                .content("Items left to buy: " + content)
                .color(255, 0, 0, 255)
                .led_color(255, 255, 255, 255)
                .time(calendar)
                .addAction(new Intent(), "Snooze", false)
                .key("test")
                .addAction(new Intent(), "Dismiss", true, false)
                .addAction(new Intent(), "Done")
                .small_icon(R.drawable.ic_notifications_icon)
                .build();

    }
}
