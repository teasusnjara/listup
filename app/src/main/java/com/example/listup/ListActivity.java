package com.example.listup;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ListActivity extends AppCompatActivity {
    private ListAdapter mAdapter;
    private SQLiteDatabase mDatabase;
    private EditText mEditTextNameList;
    private long userId = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);

        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        mDatabase = databaseHelper.getWritableDatabase();

        Bundle b = getIntent().getExtras();
        if(b != null) {
            userId = b.getLong("userId");
        }

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new ListAdapter(this, getAllLists());
        recyclerView.setAdapter(mAdapter);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT |
                ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                removeList((long) viewHolder.itemView.getTag());
            }
        }).attachToRecyclerView(recyclerView);

        mEditTextNameList = findViewById(R.id.editText_nameList);

        Button buttonCreate = findViewById(R.id.button_create);

        Button buttonLogout = findViewById(R.id.logout_button);

        buttonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createList();
            }
        });

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ListActivity.this);
                builder.setTitle("Confirmation PopUp!").setMessage("Are you sure you want to logout?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent loginIntent = new Intent(ListActivity.this, Login.class);

                        startActivity(loginIntent);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
    }

    private void createList() {

        if(mEditTextNameList.getText().toString().trim().length() == 0) {
            return;
        }

        String listName = mEditTextNameList.getText().toString();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseContract.ListEntry.COLUMN_NAME, listName);
        contentValues.put(DatabaseContract.ListEntry.COLUMN_IS_COMPLETED, false);
        contentValues.put(DatabaseContract.ListEntry.COLUMN_USER_ID, userId);

        mDatabase.insert(DatabaseContract.ListEntry.TABLE_NAME, null, contentValues);
        mAdapter.swapCursor(getAllLists());

        mEditTextNameList.getText().clear();
    }

    private void removeList(long id) {
        mDatabase.delete(DatabaseContract.ItemEntry.TABLE_NAME,
                DatabaseContract.ItemEntry.COLUMN_LIST_ID + "=" + id, null);

        mDatabase.delete(DatabaseContract.ListEntry.TABLE_NAME,
                DatabaseContract.ItemEntry._ID + "=" + id, null);

        mAdapter.swapCursor(getAllLists());
    }

    private Cursor getAllLists() {
        return mDatabase.query(
                DatabaseContract.ListEntry.TABLE_NAME,
                null,
                DatabaseContract.ListEntry.COLUMN_USER_ID + "=" + userId,
                null,
                null,
                null,
                DatabaseContract.ListEntry.COLUMN_TIMESTAMP + " DESC"
        );
    }
}
