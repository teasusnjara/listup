package com.example.listup;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class Login extends AppCompatActivity {

    EditText editTextUsername;
    EditText editTextPassword;
    Button buttonLogin;
    TextView textViewRegister;
    DatabaseHelper database;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        database = new DatabaseHelper(this);
        editTextUsername = (EditText) findViewById(R.id.username);
        editTextPassword = (EditText) findViewById(R.id.password);
        buttonLogin = (Button) findViewById(R.id.login);
        textViewRegister = (TextView) findViewById(R.id.register);

        textViewRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(Login.this, Register.class);
                startActivity(registerIntent);
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = editTextUsername.getText().toString().trim();
                String password = editTextPassword.getText().toString().trim();
                long result = database.checkUser(username, password);

                if(result != 0){
                    Intent goToList = new Intent(Login.this, ListActivity.class);

                    Bundle b = new Bundle();
                    b.putLong("userId", result);
                    goToList.putExtras(b);

                    startActivity(goToList);
                }
                else {
                    Toast.makeText(Login.this, "User does not exist!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
