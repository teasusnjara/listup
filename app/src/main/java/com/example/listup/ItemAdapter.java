package com.example.listup;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {
    private Context mContext;
    private SQLiteDatabase mDatabase;
    private Cursor mCursor;

    public ItemAdapter(Context context, Cursor cursor){
        mContext = context;
        mCursor = cursor;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView nameText;
        public TextView countText;
        public CheckBox checkBox;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            nameText = itemView.findViewById(R.id.textView_name_item);
            countText = itemView.findViewById(R.id.textView_amount_item);
            checkBox = itemView.findViewById(R.id.checkItem);

        }
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_info, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        if (!mCursor.moveToPosition(position)) {
            return;
        }

        final String name = mCursor.getString(mCursor.getColumnIndex(DatabaseContract.ItemEntry.COLUMN_NAME));
        final int amount = mCursor.getInt(mCursor.getColumnIndex(DatabaseContract.ItemEntry.COLUMN_AMOUNT));
        final long id = mCursor.getLong(mCursor.getColumnIndex(DatabaseContract.ItemEntry._ID));
        final boolean isCompleted = mCursor.getInt(mCursor.getColumnIndex(DatabaseContract.ItemEntry.COLUMN_IS_COMPLETED)) > 0;
        final long listId =  mCursor.getLong(mCursor.getColumnIndex(DatabaseContract.ItemEntry.COLUMN_LIST_ID));

        holder.nameText.setText(name);
        holder.countText.setText(String.valueOf(amount));
        holder.itemView.setTag(id);

        //in some cases, it will prevent unwanted situations
        holder.checkBox.setOnCheckedChangeListener(null);

        //if true, your checkbox will be selected, else unselected
        holder.checkBox.setChecked(isCompleted);

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //set your object's last status
                DatabaseHelper databaseHelper = new DatabaseHelper(mContext);
                mDatabase = databaseHelper.getWritableDatabase();

                setIsCompletedForItem(id, isChecked);

                int numberOfItems = mDatabase.rawQuery("SELECT * FROM item_info WHERE list_id = ?", new String[]{String.valueOf(listId)}).getCount();
                int numberOfCompletedItems = mDatabase.rawQuery("SELECT * FROM item_info WHERE list_id = ? AND is_completed = ?", new String[]{String.valueOf(listId), "1"}).getCount();

                if(numberOfItems == numberOfCompletedItems){
                    setIsCompletedForList(listId, true);
                }
                else {
                    setIsCompletedForList(listId, false);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mCursor.getCount();
    }

    public void swapCursor (Cursor newCursor) {
        if (mCursor != null) {
            mCursor.close();
        }

        mCursor = newCursor;

        if (newCursor != null) {
            notifyDataSetChanged();
        }
    }

    private void setIsCompletedForItem(long id, boolean isCompleted){
        ContentValues cv = new ContentValues();
        cv.put(DatabaseContract.ItemEntry.COLUMN_IS_COMPLETED, isCompleted);

        mDatabase.update(DatabaseContract.ItemEntry.TABLE_NAME, cv, DatabaseContract.ItemEntry._ID + "=" + id, null);
    }

    private void setIsCompletedForList(long listId, boolean isCompleted){
        ContentValues cv = new ContentValues();
        cv.put(DatabaseContract.ListEntry.COLUMN_IS_COMPLETED, isCompleted);

        mDatabase.update(DatabaseContract.ListEntry.TABLE_NAME, cv, DatabaseContract.ListEntry._ID + "=" + listId, null);
    }
}
