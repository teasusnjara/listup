package com.example.listup;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.listup.DatabaseContract.ItemEntry;
import com.example.listup.DatabaseContract.ListEntry;

public class DatabaseHelper extends SQLiteOpenHelper {
  public static final String DATABASE_NAME = "listup.db";
  public static final int DATABASE_VERSION = 1;

  public DatabaseHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    final String SQL_CREATE_LIST_TABLE = "CREATE TABLE " +
            ListEntry.TABLE_NAME + " (" +
            ListEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            ListEntry.COLUMN_NAME + " TEXT NOT NULL, " +
            ListEntry.COLUMN_IS_COMPLETED + " BOOLEAN DEFAULT NULL, " +
            ListEntry.COLUMN_TIMESTAMP + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP, " +
            ListEntry.COLUMN_USER_ID + " INTEGER NOT NULL, " +
            " FOREIGN KEY(" + ListEntry.COLUMN_USER_ID + ") REFERENCES " + DatabaseContract.UserEntry.TABLE_NAME + "(" + DatabaseContract.UserEntry._ID + ") " +
            ");";

    db.execSQL(SQL_CREATE_LIST_TABLE);

    final String SQL_CREATE_ITEM_TABLE = "CREATE TABLE " +
            ItemEntry.TABLE_NAME + " (" +
            ItemEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            ItemEntry.COLUMN_NAME + " TEXT NOT NULL, " +
            ItemEntry.COLUMN_AMOUNT + " INTEGER NOT NULL, " +
            ItemEntry.COLUMN_IS_COMPLETED + " BOOLEAN DEFAULT NULL, " +
            ItemEntry.COLUMN_TIMESTAMP + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP, " +
            ItemEntry.COLUMN_LIST_ID + " INTEGER NOT NULL, " +
            " FOREIGN KEY(" + ItemEntry.COLUMN_LIST_ID + ") REFERENCES " + ListEntry.TABLE_NAME + "(" + ListEntry._ID + ") " +
            ");";

    db.execSQL(SQL_CREATE_ITEM_TABLE);

    final String SQL_CREATE_USER_TABLE = "CREATE TABLE " +
            DatabaseContract.UserEntry.TABLE_NAME + " (" +
            DatabaseContract.UserEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DatabaseContract.UserEntry.COLUMN_USERNAME + " TEXT NOT NULL, " +
            DatabaseContract.UserEntry.COLUMN_PASSWORD + " TEXT NOT NULL" +
            ");";

    db.execSQL(SQL_CREATE_USER_TABLE);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS " + ListEntry.TABLE_NAME);
    db.execSQL("DROP TABLE IF EXISTS " + ItemEntry.TABLE_NAME);
    db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.UserEntry.TABLE_NAME);
    onCreate(db);
  }

  public long addUser (String username, String password) {
      SQLiteDatabase database = this.getWritableDatabase();

      ContentValues contentValues = new ContentValues();
      contentValues.put(DatabaseContract.UserEntry.COLUMN_USERNAME, username);
      contentValues.put(DatabaseContract.UserEntry.COLUMN_PASSWORD, password);

      long result = database.insert(DatabaseContract.UserEntry.TABLE_NAME, null, contentValues);
      database.close();

      return result;
  }

  public long checkUser (String username, String password){
      SQLiteDatabase database = this.getReadableDatabase();

      String selection = DatabaseContract.UserEntry.COLUMN_USERNAME + "=?" + " and " + DatabaseContract.UserEntry.COLUMN_PASSWORD + "=?";
      String[] selectionArgs = { username, password };

      Cursor cursor = database.query(DatabaseContract.UserEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

      long id = 0;
      if(cursor.moveToFirst()) {
          id = cursor.getLong(cursor.getColumnIndex(DatabaseContract.UserEntry._ID));
      }

      cursor.close();
      database.close();

      return id;
  }
}
